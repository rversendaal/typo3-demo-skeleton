<?php
defined('TYPO3_MODE') or die ('Access denied.');

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['BE'], [
    'debug' => 0,
]);

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['FE'], [
    'debug' => 0,
]);

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['SYS'], [
    'clearCacheSystem' => true,
    'cookieSecure' => true,
    'cookieHttpOnly' => false,
    'displayErrors' => 0,
    'exceptionalErrors' => 20480,
    'sqlDebug' => 0,
    'systemLogLevel' => 2
]);

