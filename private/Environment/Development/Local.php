<?php
defined('TYPO3_MODE') or die ('Access denied.');

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['BE'], [
    'debug' => 1,
]);

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['FE'], [
    'debug' => 1,
]);

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['SYS'], [
    'caching' => [
        'cacheConfigurations' => [
            'cache_core' => [
                'backend' => '\TYPO3\\CMS\\Core\\Cache\\Backend\\NullBackend'
            ],
            'extbase_object' => [
                'backend' => '\TYPO3\\CMS\\Core\\Cache\\Backend\\NullBackend'
            ],
            'extbase_reflection' => [
                'backend' => '\TYPO3\\CMS\\Core\\Cache\\Backend\\NullBackend'
            ]
        ]
    ],
    'clearCacheSystem' => true,
    'cookieSecure' => false,
    'cookieHttpOnly' => false,
    'displayErrors' => 1,
    'exceptionalErrors' => 28674,
    'sqlDebug' => 1,
    'systemLogLevel' => 0

]);