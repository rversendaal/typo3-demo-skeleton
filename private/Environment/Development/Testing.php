<?php
defined('TYPO3_MODE') or die ('Access denied.');

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['BE'], [
    'debug' => 1,
]);

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['FE'], [
    'debug' => 1,
]);

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TYPO3_CONF_VARS']['SYS'], [
    'clearCacheSystem' => true,
    'cookieSecure' => false,
    'cookieHttpOnly' => false,
    'displayErrors' => 1,
    'exceptionalErrors' => 28674,
    'sqlDebug' => 1,
    'systemLogLevel' => 0
]);

