DELETE FROM be_users WHERE username = 'redkiwi';
INSERT INTO be_users (username, password, admin, disable) VALUES ('redkiwi', 'redkiwi', 1, 0);
TRUNCATE TABLE sys_domain;
INSERT INTO sys_domain
    ( pid, cruser_id, hidden, domainName, redirectHttpStatusCode, sorting )
VALUES
    ( 1, 1, 0, '%VAGRANT_HOSTNAME%', 301, 1 );
QUIT