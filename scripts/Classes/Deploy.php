<?php
namespace Redkiwi\Deploy;

use Composer\Script\Event;

/**
 * deploy
 *
 * @package Redkiwi\Deploy
 */
class Deploy
{

    /**
     * @param Event $event
     */
    public static function createProject(Event $event)
    {
        $projectStartup = new ProjectStartup($event);
        $projectStartup->run();
    }
}