<?php
namespace Redkiwi\Deploy;

use Composer\Script\Event;

/**
 * project startup
 *
 * @package Redkiwi\Deploy
 */
class ProjectStartup
{
    /**
     * The event
     *
     * @var Composer\Script\Event
     */
    protected $event = null;

    /**
     * ProjectStartup constructor.
     * @param Composer\Script\Event $event
     */
    public function __construct($event)
    {
        $this->event = $event;
    }

    /**
     * Ask the questions
     */
    public function run()
    {
        $data = [
            '%VAGRANT_IP%' => $this->getVagrantIp(),
            '%VAGRANT_HOSTNAME%' => $this->getVagrantHostname(),
            '%VAGRANT_NAME%' => $this->getVagrantBoxname(),
            '%REMOTE_HOST%' => $this->getRemoteHostname(),
            '%REMOTE_USERNAME%' => $this->getRemoteUsername()
        ];

        $this->process($data);
    }

    /**
     * @param array $data
     */
    public function process(array $data)
    {
        $files = [
            'vagrant' => 'Vagrantfile',
            'sql' => 'private/ScriptConfiguration/seeder.sql',
            'sync' => 'private/pullit/project.json'
        ];

        foreach ($files as $file) {
            $file_contents = str_replace(array_keys($data), array_values($data), file_get_contents($file));
            file_put_contents($file, $file_contents);
        }
    }

    /**
     * @return string
     */
    public function getVagrantIp()
    {
        $data = $this->event->getIO()->askAndValidate('Enter the vagrant ip (192.168.xx.xx): ', function ($value) {
            if (!filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) || substr($value, 0, 8) !== '192.168.') {
                throw new \Exception('$value is not a valid internal network IP address');
            }
            return $value;
        }, 3, 'unknown');

        return $data;
    }

    /**
     * @return string
     */
    public function getVagrantHostname()
    {
        $data = $this->event->getIO()->askAndValidate('Enter the vagrant hostname (dev.WEBSITE.com): ',
            function ($value) {
                if (!preg_match('|^([a-z])+.([a-z0-9\.])+.([a-z])+$|', $value)) {
                    throw new \Exception('$value is not a valid domain');
                }
                return $value;
            }, 3, 'unknown');

        return $data;
    }

    /**
     * @return string
     */
    public function getVagrantBoxname()
    {
        $data = $this->event->getIO()->askAndValidate('Enter the vagrant box name (unknown_box): ', function ($value) {
            if (!preg_match('|^([a-z0-9])+$|', $value)) {
                throw new \Exception('$value is not a valid box name');
            }

            return $value;
        }, 3, 'unknown');

        return $data;
    }

    /**
     * @return string
     */
    public function getRemoteHostname()
    {
        $data = $this->event->getIO()->askAndValidate('Enter the remote host (test.website.typocloud.nl): ',
            function ($value) {
                if (!preg_match('|^([a-z])+.([a-z0-9\.])+.([a-z])+$|', $value)) {
                    throw new \Exception('$value is not a valid remote host');
                }
                return $value;
            }, 3, 'unknown');

        return $data;
    }

    /**
     * @return string
     */
    public function getRemoteUsername()
    {
        $data = $this->event->getIO()->askAndValidate('Enter the remote username (tctest): ', function ($value) {
            if (!preg_match('|^([a-z0-9])+$|', $value)) {
                throw new \Exception('$value is not a valid remote username');
            }

            return $value;
        }, 3, 'unknown');

        return $data;
    }
}