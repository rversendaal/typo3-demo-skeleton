clear

echo "------------------------------------------------------------------------------------------------------------------------"
echo "Local Environment Startup Script v0.1"
echo ""
echo "This will synchronize your local resources with the remote and OVERWRITE your entire local database. Use this script"
echo "with caution and at your own risk!"
echo ""
echo "Made by Redkiwi"
echo "------------------------------------------------------------------------------------------------------------------------"
echo ""

################################
# Getting the needed variables #
################################
mysql=$(which mysql)
awk=$(which awk)
grep=$(which grep)
remote_configuration="private/ScriptConfiguration/Server.cfg"
local_configuration="private/ScriptConfiguration/Synchronization.cfg"

echo "Loading local configuration.."
if [ ! -f "${local_configuration}" ]; then
	echo "No configuration file found on local, aborting!"
	exit;
fi
source ${local_configuration}

echo "Synchronizing and loading remote configuration.."
scp -q ${remote_username}@${remote_host}:${remote_path}private/ScriptConfiguration/Server.cfg ${remote_configuration}
if [ ! -f "${remote_configuration}" ]; then
	echo "No synchronization configuration found on remote, aborting!"
	exit;
fi
source ${remote_configuration}
echo ""

############################
# Resource synchronization #
############################
echo "Running file synchronization.."
for ((i=0; i<${#synchronization_paths[@]}; i++))
do
	if [ ! -d "${synchronization_paths[${i}]}" ]; then
		mkdir -p "${synchronization_paths[${i}]}";
	fi
	rsync -avz -e ssh ${remote_username}@${remote_host}:${remote_path}${synchronization_paths[${i}]} ${synchronization_paths[${i}]}
done
echo "The files are done!"
echo ""

############################
# Database synchronization #
############################

dump_file="backup"
dump_file="${dump_file}.sql"
echo "Dumping remote database into ${dump_file}.."
ssh ${remote_username}@${remote_host} "mysqldump -u${environment_database_username} -p${environment_database_password} -h${environment_database_host} ${environment_database} > ${dump_file}"

echo "Downloading dump ${dump_file}.."
scp ${remote_username}@${remote_host}:${dump_file} ${dump_file}
echo "Removing remote dump file.."
ssh ${remote_username}@${remote_host} "rm ${dump_file}"
if [ ! -f "${dump_file}" ]; then
	echo "Database dump failed or was not downloaded, aborting!"
	exit;
fi
echo ""

echo "Dropping local tables.."
tables=$($mysql -u$database_username -p$database_password -h$database_host $database -e 'SHOW TABLES' | $awk '{ print $1}' | $grep -v '^Tables' )
for table in $tables
do
	$mysql -u$database_username -p$database_password -h$database_host $database -e "DROP TABLE $table"
done
echo "Done!"
echo ""

echo "Importing new dump.."
$mysql -u$database_username -p$database_password -h$database_host $database < $dump_file
echo "Done!"
echo ""

echo "Removing local dump file.."
rm $dump_file
echo "Done!"

echo "Adding/updating Redkiwi user and local domain"
$mysql -u$database_username -p$database_password -h$database_host $database < private/ScriptConfiguration/seeder.sql
echo "Done!"
echo ""

echo "Adding the default .htaccess if it didn't exist yet";
cp -n vendor/typo3/cms/_.htaccess www/.htaccess
echo "Done!"
echo ""

echo "Added the default Configuration.php if if didn't exist yet";
cp -n private/Environment/Configuration.php.dist private/Environment/Configuration.php
echo "Done!"
echo ""

echo "Build all needed dependencies"
cd build/
yarn install
gulp deploy
echo "Done!"

echo "All done!"