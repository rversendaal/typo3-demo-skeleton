#!/bin/bash
cd ..

#
# Writing version release
#
echo "<?php return '$1';" > release

#
# Composer
#
echo "Installing composer components.."
composer install
echo "Done!"
echo ""

#
# Frontend gulp & bower
#
echo "Build all needed dependencies"
cd ./build/
yarn install
echo "Done!"
echo ""
echo "Running gulp file.."
gulp deploy
echo "Done!"
echo ""

#
# TYPO3
#
cd ../../www/
echo "Correcting permissions.."
chmod +x typo3/cli_dispatch.phpsh
echo "Done"
echo ""

echo "Performing database compare.. (add field, change fields, add tables)"
./typo3/cli_dispatch.phpsh extbase databaseapi:databasecompare 2,3,4
echo "Done!"
echo ""

echo "Clearing caches.."
./typo3/cli_dispatch.phpsh extbase cacheapi:clearallactiveopcodecache
./typo3/cli_dispatch.phpsh extbase cacheapi:clearallcaches
./typo3/cli_dispatch.phpsh extbase cacheapi:clearsystemcache
./typo3/cli_dispatch.phpsh extbase cacheapi:clearconfigurationcache
rm -rf typo3temp/var/Cache
echo "Done!"
echo ""

echo "All done, bye!"

