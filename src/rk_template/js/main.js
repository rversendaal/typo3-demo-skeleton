$ = require('../../../build/node_modules/jquery/dist/jquery.min.js');

Site = {

    init: () => {
        $('#verifyPhone').on('submit', (e) => {
            e.preventDefault();

            let formData = $('form').serializeArray();
            let errorDiv = $('#verifyPhone').find('#errors');
            let resultDiv = $('#verifyPhone').find('#results');

            $.post('index.php?eID=ronanverify_check', {formData: formData}, (data) => {
                let res = JSON.parse(data);

                if(res.success === false) {
                    errorDiv.html(res.error.info);
                } else {
                    if (res.valid === true){
                        resultDiv.html(JSON.stringify(res, undefined, 2));
                    }
                    errorDiv.html('');
                }
            }).fail((err) => {
                console.table(err);
            });
        })
    }
};

(function(){
    Site.init();
})();