<?php
defined('TYPO3_MODE') or die ('Access denied.');

if (!defined('PATH_site_root')) {
    define('PATH_site_root', dirname(realpath(PATH_site)));
}

/**
 * Load CONTEXT dependent configuration. Keep in mind to move the configuration
 * to the correct configuration file if a environment dependent setting in the
 * INSTALL TOOL is changed.
 */
switch (\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()) {

    case 'Development':
    case 'Development/Local':
        require_once(PATH_site_root . '/private/Environment/Development/Local.php');
        break;

    case 'Development/Testing':
        require_once(PATH_site_root . '/private/Environment/Development/Testing.php');
        break;

    case 'Production/Staging':
        require_once(PATH_site_root . '/private/Environment/Production/Staging.php');
        break;

    default:
        require_once(PATH_site_root . '/private/Environment/Production/Live.php');
}

if (file_exists(PATH_site_root . '/private/Environment/Configuration.php')) {
    require_once(PATH_site_root . '/private/Environment/Configuration.php');
}

// Display current release
if (is_file(PATH_site_root . '/release')) {
    $release = include PATH_site_root . '/release';
    if (!empty($release)) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = trim($GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename']) . ' [' . substr($release, 0, 8) . ']';
    }
}
