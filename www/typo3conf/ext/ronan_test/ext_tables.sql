#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
    tx_ronantest_cta_image int(1) DEFAULT 0 NOT NULL,
    tx_ronantest_cta_title varchar(255) DEFAULT '' NOT NULL,
    tx_ronantest_cta_description varchar(255) DEFAULT '' NOT NULL,
    tx_ronantest_cta_link varchar(255) DEFAULT '' NOT NULL,
    tx_ronantest_cta_link_text varchar(255) DEFAULT '' NOT NULL,
);