<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Ronan\'s Extension',
    'description' => 'Ronan\'s first extension',
    'category' => 'extension',
    'author' => 'Ronan Versendaal',
    'author_company' => 'Redkiwi.',
    'author_email' => 'versendaal@redkiwi.nl',
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'version' => '0.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99',
        ]
    ],
    'autoload-dev' => [
        'psr-4' => [
            'Redkiwi\\RonanTest\\Tests\\' => 'Tests',
        ],
    ],
];