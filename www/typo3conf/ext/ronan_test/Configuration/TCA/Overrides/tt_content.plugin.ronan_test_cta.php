<?php

defined('TYPO3_MODE') or die('Access denied');

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

call_user_func(function ($extension, $plugin, $pluginName) {
    $extensionLanguageFilePrefix = 'LLL:EXT:' . $extension . '/Resources/Private/Language/' . $pluginName . '.xlf';

    ExtensionManagementUtility::addPlugin(
        [
            $extensionLanguageFilePrefix . ':cta.name',
            $plugin,
            'EXT:' . $extension . '/ext_icon.png'
        ],
        ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT,
        $extension
    );

    ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'call_to_action',
        implode(
            ',',
            [
                'tx_ronantest_cta_title;' . $extensionLanguageFilePrefix . ':cta.title',
                'tx_ronantest_cta_link;' . $extensionLanguageFilePrefix . ':cta.link',
                'tx_ronantest_cta_link_text;' . $extensionLanguageFilePrefix . ':cta.linkText',
                '--linebreak--',
                'tx_ronantest_cta_description;' . $extensionLanguageFilePrefix . ':cta.description',
                '--linebreak--',
                'tx_ronantest_cta_image;' . $extensionLanguageFilePrefix . ':cta.image'
            ]
        )
    );

    // @todo translate the palette names
    $GLOBALS['TCA']['tt_content']['types'][$plugin] = [
        'showitem' => '
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.general;general,
            --div--;Configure,
                --palette--;Configure;call_to_action,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.access,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.visibility;visibility,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.access;access'
    ];
}, 'ronan_test', 'ronan_test_cta', 'CallToAction');
