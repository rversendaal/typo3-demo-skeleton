<?php

defined('TYPO3_MODE') or die('Access denied.');

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(function ($extension, $table, $pluginName) {
    $extensionLanguageFilePrefix = 'LLL:EXT:' . $extension . '/Resources/Private/Language/' . $pluginName . '.xlf';

    $columns = [
        'tx_ronantest_cta_title' => [
            'label' => $extensionLanguageFilePrefix . ':cta.name',
            'config' => [
                'type' => 'input',
                'max' => 40,
                'eval' => 'trim'
            ]
        ],
        'tx_ronantest_cta_description' => [
            'label' => $extensionLanguageFilePrefix . ':cta.description',
            'config' => [
                'type' => 'text',
                'max' => 60,
                'eval' => 'trim'
            ]
        ],
        'tx_ronantest_cta_image' => [
            'label' => $extensionLanguageFilePrefix . ':cta.image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('tx_ronantest_cta_image', [
                'foreign_match_fields' => [
                    'fieldname' => 'tx_ronantest_cta_image',
                    'tablenames' => $table,
                    'table_local' => 'sys_file',
                ],
                'overrideChildTca' => [
                    'types' => [
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                    ],
                ],
                'minitems' => 0,
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'])
        ],
        'tx_ronantest_cta_link' => [
            'label' => $extensionLanguageFilePrefix . ':cta.link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink'
            ]
        ],
        'tx_ronantest_cta_link_text' => [
            'label' => $extensionLanguageFilePrefix . ':cta.linkText',
            'config' => [
                'type' => 'input',
            ]
        ]
    ];

    ExtensionManagementUtility::addTCAcolumns($table, $columns);
}, 'ronan_test', 'tt_content', 'CallToAction');
