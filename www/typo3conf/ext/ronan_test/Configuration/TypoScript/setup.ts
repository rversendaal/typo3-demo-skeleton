plugin.tx_ronantest {
    view {
        layoutRootPaths {
            10 = {$plugin.tx_ronantest.view.layoutRootPath}
        }
        partialRootPaths {
            10 = {$plugin.tx_ronantest.view.partialRootPath}
        }
        templateRootPaths {
            10 = {$plugin.tx_ronantest.view.templateRootPath}
        }
    }
}

tt_content {

    ronan_test_cta =< lib.contentElement
    ronan_test_cta {
        layoutRootPaths.5 = {$plugin.tx_ronantest.view.layoutRootPath}
        partialRootPaths.5 = {$plugin.tx_ronantest.view.partialRootPath}
        templateRootPaths.5 = {$plugin.tx_ronantest.view.templateRootPath}


        dataProcessing {
            100 = Redkiwi\RonanTest\DataProcessing\CallToActionDataProcessor
        }

        templateName = CallToAction


    }
}