plugin.tx_ronantest {
    view {
        layoutRootPath = EXT:ronan_test/Resources/Private/Layouts
        partialRootPath = EXT:ronan_test/Resources/Private/Partials
        templateRootPath = EXT:ronan_test/Resources/Private/Templates
    }
}