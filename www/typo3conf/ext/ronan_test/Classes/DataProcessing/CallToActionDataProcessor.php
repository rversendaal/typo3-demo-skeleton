<?php
namespace Redkiwi\RonanTest\DataProcessing;

use Redkiwi\RonanTest\Domain\Model\CallToAction;
use Redkiwi\RkCore\Traits\ObjectManagerTrait;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Class CallToActionDataProcessor
 * @package Redkiwi\RonanTest\DataProcessing
 */
class CallToActionDataProcessor implements DataProcessorInterface
{
    use ObjectManagerTrait;

    /**
     * @var null
     */
    private $dataMapper = null;

    public function __construct()
    {
        $this->dataMapper = $this->getObjectManager()->get(DataMapper::class);
    }

    /**
     * @param ContentObjectRenderer $cObj
     * @param array $contentObjectConfiguration
     * @param array $processorConfiguration
     * @param array $processedData
     * @return array
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) : array
    {
        $processedData['content'] = $this->getDataMapper()->map(
            CallToAction::class,
            [$processedData['data']]
        )[0];

        return $processedData;
    }

    /**
     * @return null
     */
    public function getDataMapper() : DataMapper
    {
        return $this->dataMapper;
    }
}