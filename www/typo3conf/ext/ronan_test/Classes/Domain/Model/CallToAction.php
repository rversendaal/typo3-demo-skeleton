<?php
namespace Redkiwi\RonanTest\Domain\Model;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class CallToAction
 * @package Redkiwi\RonanTest\Domain\Model
 */
class CallToAction extends AbstractEntity
{
    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $ctaImage = null;

    /** @var string */
    protected $ctaTitle = '';

    /** @var string  */
    protected $ctaDescription = '';

    /** @var string  */
    protected $ctaLink = '';

    /** @var string */
    protected $ctaLinkText = '';

    /**
     * @return FileReference
     */
    public function getCtaImage(): FileReference
    {
        if ($this->ctaImage === null) {
            $this->ctaImage = GeneralUtility::makeInstance(FileReference::class);
        }

        return $this->ctaImage;
    }

    /**
     * @return string
     */
    public function getCtaTitle(): string
    {
        return $this->ctaTitle;
    }

    /**
     * @return string
     */
    public function getCtaDescription(): string
    {
        return $this->ctaDescription;
    }

    /**
     * @return string
     */
    public function getCtaLink(): string
    {
        return $this->ctaLink;
    }

    /**
     * @return string
     */
    public function getCtaLinkText(): string
    {
        return $this->ctaLinkText;
    }
}
