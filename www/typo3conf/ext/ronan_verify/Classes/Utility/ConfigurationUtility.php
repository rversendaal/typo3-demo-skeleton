<?php
namespace Redkiwi\RonanVerify\Utility;

/**
 * Class ConfigurationUtility
 * @package Redkiwi\RonanVerify\Utility
 */
class ConfigurationUtility
{
    const EXTENSION_NAME = 'ronan_verify';

    /** @var null */
    protected static $configuration = null;

    /**
     * @return array
     */
    public static function getConfiguration(): array
    {
        if (static::$configuration === null) {
            $data = $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][static::EXTENSION_NAME];
            if (!is_array($data)) {
                static::$configuration = (array)unserialize($data);
            } else {
                static::$configuration = $data;
            }
        }

        return static::$configuration;
    }

    /**
     * @param string $key
     * @return array
     */
    protected static function getKey(string $key): array
    {
        $configuration = static::getConfiguration();

        return (!empty($configuration) && isset($configuration[$key])) ? $configuration[$key] : [];
    }

    /**
     * @return array
     */
    public static function getApiServiceOptions(): array
    {
        $verifyServiceData = static::getKey('verifyService');

        return (array) (isset($verifyServiceData['options'])) ? $verifyServiceData['options'] : [];
    }
}
