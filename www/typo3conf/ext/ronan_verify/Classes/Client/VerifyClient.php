<?php namespace Redkiwi\RonanVerify\Client;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class VerifyClient
 * @package Redkiwi\RonanVerify\Client
 */
class VerifyClient
{

    /** @var Client */
    private $client;

    /** @var string */
    private $access_key;

    /**
     * VerifyClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://apilayer.net/api',
        ]);
    }

    /**
     * @param string $access_key
     */
    public function setAccessKey(string $access_key)
    {
        $this->access_key = $access_key;
    }

    /**
     * @param string $number
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function verifyNumberRequest(string $number): ResponseInterface
    {
        return $this->client->get(
            'validate',
            [
                'query' => array_merge(
                    [
                        'number' => $number,
                        'access_key' => $this->access_key
                    ]
                )
            ]
        );
    }
}
