<?php namespace Redkiwi\RonanVerify\Controller;

use \Redkiwi\RkCore\Traits\ObjectManagerTrait;
use Redkiwi\RonanVerify\Client\VerifyClient;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class VerificationController
 * @package Redkiwi\RonanVerify\Controller
 */
class VerificationController extends ActionController
{
    use ObjectManagerTrait;

    /** @var VerifyClient */
    private $client;

    /**
     * VerificationController constructor.
     * @param VerifyClient $client
     */
    public function __construct(VerifyClient $client)
    {
        $this->client = $client;

        parent::__construct();
    }

    public function listAction()
    {

        // Assign  arguments as vars in view.
        $includedKeys = ['error', 'message'];

        foreach ($this->request->getArguments() as $key => $argument) {
            if (in_array($key, $includedKeys)) {
                $this->view->assign($key, $argument);
            }
        }
    }

    public function verifyAction()
    {
        $this->client->setAccessKey($this->settings['flexform']['settings']['api']);

        if ($this->request->hasArgument('phonenumber') === false) {

            // Redirect to listAction
            $this->redirect('list', null, null, ['error' => true, 'message' => 'No phone number specified']); //@todo Translate this message
        }

        $request = $this->client->verifyNumberRequest($this->request->getArgument('phonenumber'));

        $result = json_decode($request->getBody()->getContents());

        if ($this->hasValidResult($result) === false) {
            $this->redirect('list', null, null, ['error' => true, 'message' => 'Invalid phone number']); //@todo Translate this message
        }

        $this->view->assign('result', $result);
    }

    /**
     * @param $result
     * @return bool
     */
    private function hasValidResult($result): bool
    {
        return (isset($result) && $result->valid);
    }
}
