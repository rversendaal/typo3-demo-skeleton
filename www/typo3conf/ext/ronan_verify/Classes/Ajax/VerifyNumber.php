<?php
namespace Redkiwi\RonanVerify\Ajax;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Redkiwi\RonanVerify\Client\VerifyClient;
use Redkiwi\RonanVerify\Utility\ConfigurationUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class VerifyNumber
 * @package Redkiwi\RonanVerify\Ajax
 */
class VerifyNumber
{
    /**
     * VerifyNumber constructor.
     */
    public function __construct()
    {
        $this->client = GeneralUtility::makeInstance(VerifyClient::class);
    }

    /**
     * Checks if the current request type is supported by the controller.
     *
     * @param ServerRequestInterface $request The current request
     * @return void TRUE if this request type is supported, otherwise FALSE
     * @api
     */
    public function canProcessRequest(ServerRequestInterface $request)
    {
        // TODO: Implement canProcessRequest() method.
    }

    /**
     * Processes a general request. The result can be returned by altering the given response.
     *
     * @param ServerRequestInterface $request The request object
     * @param ResponseInterface $response The response, modified by the controller
     * @return ResponseInterface
     * @throws Exception
     * @api
     */
    public function processRequest(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $params = [];
        $formData = GeneralUtility::_POST('formData');
        $accessKey = ConfigurationUtility::getApiServiceOptions()['access_key'];

        if (!isset($accessKey)) {
            throw new Exception('No access key has been set!');
        }

        foreach ($formData as $index => $field) {
            $params[$field['name']] = $field['value'];
        }

        $this->client->setAccessKey($accessKey);

        $verifyRequest = $this->client->verifyNumberRequest($params['tx_ronanverify_verify_phone[phonenumber]']);

        $response->getBody()->write($verifyRequest->getBody()->getContents());

        return $response;
    }
}
