<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Ronan\'s Phone number verifier',
    'description' => 'A plugin that verifies phonenumbers by using the numverify API',
    'category' => 'plugin',
    'author' => 'Ronan Versendaal',
    'author_company' => 'Redkiwi.',
    'author_email' => 'versendaal@redkiwi.nl',
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'version' => '0.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99',
        ]
    ],
    'autoload' => [
        'psr-4' => [
            'Redkiwi\\RonanVerify\\' => 'Classes'
        ]
    ],
    'autoload-dev' => [
        'psr-4' => [
            'Redkiwi\\RonanVerify\\Tests\\' => 'Tests',
        ],
    ],
];