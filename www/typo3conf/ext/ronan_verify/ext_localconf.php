<?php

defined('TYPO3_MODE') or die('Access denied.');

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['ronanverify_check'] = Redkiwi\RonanVerify\Ajax\VerifyNumber::class . '::processRequest';

call_user_func(function ($extension, $plugin) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $extension . '/Configuration/TSconfig/include_page.ts">');

    // Configures the plugin
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Redkiwi.' . $extension,
        $plugin,
        [   'Verification' => 'list, verify'],
        // non-cacheable actions
        [   'Verification' => 'verify'      ]
    );
}, 'ronan_verify', 'verify_phone');
