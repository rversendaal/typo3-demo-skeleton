plugin.tx_ronanverify {
    view {
        layoutRootPaths {
            10 = {$plugin.tx_ronanverify.view.layoutRootPath}
        }
        partialRootPaths {
            10 = {$plugin.tx_ronanverify.view.partialRootPath}
        }
        templateRootPaths {
            10 = {$plugin.tx_ronanverify.view.templateRootPath}
        }
    }
}
