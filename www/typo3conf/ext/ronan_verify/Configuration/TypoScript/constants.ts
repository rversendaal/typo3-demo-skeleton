plugin.tx_ronanverify {
    view {
        layoutRootPath = EXT:ronan_verify/Resources/Private/Layouts
        partialRootPath = EXT:ronan_verify/Resources/Private/Partials
        templateRootPath = EXT:ronan_verify/Resources/Private/Templates
    }
}