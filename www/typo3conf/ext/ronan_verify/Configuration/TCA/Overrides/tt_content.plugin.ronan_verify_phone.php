<?php

defined('TYPO3_MODE') or die('Access denied');

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

call_user_func(function ($extension, $plugin, $pluginName) {
    $extensionLanguageFilePrefix = 'LLL:EXT:' . $extension . '/Resources/Private/Language/' . $pluginName . '.xlf';

    ExtensionManagementUtility::addPlugin(
        [
            $extensionLanguageFilePrefix . ':plugin.name',
            $plugin,
            // Icon
        ],
        ExtensionUtility::PLUGIN_TYPE_PLUGIN,
        $extension
    );

    ExtensionManagementUtility::addPiFlexFormValue(
        $plugin,
        'FILE:EXT:ronan_verify/Configuration/FlexForms/' . $pluginName . '.xml'
    );

    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$plugin] = 'pi_flexform';
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$plugin] = 'pages,recursive';
}, 'ronan_verify', 'ronanverify_verify_phone', 'VerifyPhone');
