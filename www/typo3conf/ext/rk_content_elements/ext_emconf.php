<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'rk_template: Content Elements',
    'description' => 'Content Elements for rk_template website',
    'category' => 'template',
    'author' => 'Ted van Diepen',
    'author_email' => 'vandiepen@redkiwi.nl',
    'state' => 'stable',
    'uploadFolder' => false,
    'clearCacheOnLoad' => true,
    'author_company' => 'Redkiwi',
    'version' => '8.7.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
            'extbase' => '8.7.0-8.7.99'
        ],
        'conflicts' => [],
        'suggests' => []
    ],
    'autoload' => [
        'psr-4' => [
            'Redkiwi\\RkContentElements\\' => 'Classes',
        ],
    ],
    'autoload-dev' => [
        'psr-4' => [
            'Redkiwi\\RkContentElements\\Tests\\' => 'Tests',
        ],
    ],
];
