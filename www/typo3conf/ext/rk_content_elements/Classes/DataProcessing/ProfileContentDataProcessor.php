<?php namespace Redkiwi\RkContentElements\DataProcessing;

use Redkiwi\RkContentElements\Domain\Model\Profile;
use Redkiwi\RkCore\Traits\ObjectManagerTrait;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Class ProfileContentDataProcessor
 * @package Redkiwi\RkContentElements\DataProcessing
 */
class ProfileContentDataProcessor implements DataProcessorInterface
{
    use ObjectManagerTrait;

    /** @var DataMapper */
    private $dataMapper = null;

    /**
     * ProfileContentDataProcessor constructor.
     */
    public function __construct()
    {
        $this->dataMapper = $this->getObjectManager()->get(DataMapper::class);
    }

    /**
     * Process content object data
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {

        $processedData['content'] = $this->dataMapper->map(
            Profile::class,
            [$processedData['data']]
        )[0];

        return $processedData;
    }
}
