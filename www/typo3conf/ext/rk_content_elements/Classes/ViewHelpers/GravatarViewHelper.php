<?php
namespace Redkiwi\RkContentElements\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Fluid\View\StandaloneView;

class GravatarViewHelper extends AbstractViewHelper
{
    /** @var string */
    const GRAVATAR_URL = 'https://gravatar.com/avatar/';

    /** @var bool */
    protected $escapeOutput = false;

    /** @var string */
    private $sourceUrl = '';

    /**
     * @param string $email
     * @param string $class
     * @return string
     */
    public function render(string $email, string $class = ''): string
    {
        if ($this->isOutputEscapingEnabled() === false) {
            // Strips tags from input when $escapeOutput is false
            $email = strip_tags($email);
        }

        $hash = md5(strtolower(trim($email)));
        $this->setSourceUrl($hash);

        return $this->buildImage($class);
    }

    /**
     * @param string $class
     * @return string
     */
    private function buildImage(string $class): string
    {
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->setTemplatePathAndFilename('EXT:rk_content_elements/Resources/Private/Templates/Standalone/Image.html');

        $view->assignMultiple([
            'source' => $this->sourceUrl,
            'class' => $class
        ]);

        return $view->render();
    }

    /**
     * @param string $sourceUrl
     */
    private function setSourceUrl(string $sourceUrl): void
    {
        $this->sourceUrl = self::GRAVATAR_URL . $sourceUrl;
    }
}
