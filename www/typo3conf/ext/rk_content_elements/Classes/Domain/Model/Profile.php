<?php
namespace Redkiwi\RkContentElements\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Profile
 * @package Redkiwi\RkContentElements\Domain\Model
 */
class Profile extends AbstractEntity
{
    /** @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser */
    protected $user = null;

    /**
     * @return FrontendUser
     */
    public function getUser(): FrontendUser
    {
        return $this->user;
    }
}
