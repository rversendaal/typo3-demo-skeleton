<?php
defined('TYPO3_MODE') or die('Access denied.');

call_user_func(function ($extension) {
    if (TYPO3_MODE === 'BE') {

        /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

        $iconRegistry->registerIcon(
            'ext-rkcontentelements_quote-wizard-icon',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rk_content_elements/Resources/Backend/Icons/Wizards/Quote.svg']
        );

        $iconRegistry->registerIcon(
            'ext-rkcontentelements_profile_card-wizard-icon',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rk_content_elements/Resources/Backend/Icons/Wizards/ProfileCard.svg']
        );
    }
}, $_EXTKEY);
