<?php
defined('TYPO3_MODE') or die('Access denied.');

call_user_func(function ($extension) {

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $extension . '/Configuration/TSconfig/include_page.ts">');

}, $_EXTKEY);
