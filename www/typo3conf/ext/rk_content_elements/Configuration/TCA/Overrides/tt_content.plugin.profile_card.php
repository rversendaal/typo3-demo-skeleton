<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3_MODE') or die('Access denied.');

call_user_func(function ($extension, $plugin, $pluginName) {
    $extensionLanguageFilePrefix = 'LLL:EXT:' . $extension . '/Resources/Private/Language/Plugin/' . $pluginName . '.xlf:';
    $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    ExtensionManagementUtility::addPlugin(
        [
            $extensionLanguageFilePrefix . 'title',
            $plugin,
            'EXT:' . $extension . '/Resources/Backend/Icons/ContentElements/' . $pluginName . '.svg'
        ],
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT,
        $extension
    );

    ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        $plugin . '_user',
        implode(',', [
            'tx_rkcontentelements_profile_card_user',
        ])
    );

    $GLOBALS['TCA']['tt_content']['types'][$plugin] = [
        'showitem' => '
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                --palette--;' . $frontendLanguageFilePrefix . 'palette.headers;simple_header,
            --div--;' . $extensionLanguageFilePrefix . 'title,
                --palette--;' . $extensionLanguageFilePrefix . 'palette.content;' . $plugin . '_user,'
    ];
}, 'rk_content_elements', 'rkcontentelements_profile_card', 'ProfileCard');
