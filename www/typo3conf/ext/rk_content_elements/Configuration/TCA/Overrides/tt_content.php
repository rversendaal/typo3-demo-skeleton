<?php
defined('TYPO3_MODE') or die('Access denied.');

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(function ($extension, $table) {
    $extensionLanguageFilePrefix = 'LLL:EXT:' . $extension . '/Resources/Private/Language/Plugin/';

    ExtensionManagementUtility::addTCAcolumns(
        $table,
        [
            'tx_rkcontentelements_profile_card_user' => [
                'exclude' => false,
                'label' => $extensionLanguageFilePrefix . 'ProfileCard.xlf:column.user',
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'fe_users',
                    'items' => [],
                    'minitems' => 1
                ]
            ]
        ]
    );
}, 'rk_content_elements', 'tt_content');
