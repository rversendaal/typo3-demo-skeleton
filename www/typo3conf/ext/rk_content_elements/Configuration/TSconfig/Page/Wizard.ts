mod.wizards.newContentElement.wizardItems {

    # Add custom elements for rk_template content elements first
    _rk {
        header = LLL:EXT:rk_content_elements/Resources/Private/Language/Model/ContentElements.xlf:wizard.title
        elements {

            rkcontentelements_quote {
                iconIdentifier = ext-rkcontentelements_quote-wizard-icon
                title = LLL:EXT:rk_content_elements/Resources/Private/Language/Model/ContentElements.xlf:quote.wizard.title
                description = LLL:EXT:rk_content_elements/Resources/Private/Language/Model/ContentElements.xlf:quote.wizard.description
                tt_content_defValues {
                    CType = rkcontentelements_quote
                }
            }

            rkcontentelements_profile_card {
                iconIdentifier = ext-rkcontentelements_profile_card-wizard-icon
                title = LLL:EXT:rk_content_elements/Resources/Private/Language/Model/ContentElements.xlf:profile_card.wizard.title
                description = LLL:EXT:rk_content_elements/Resources/Private/Language/Model/ContentElements.xlf:profile_card.wizard.description
                tt_content_defValues {
                    CType = rkcontentelements_profile_card
                }
            }

        }

        show = *
    }

}