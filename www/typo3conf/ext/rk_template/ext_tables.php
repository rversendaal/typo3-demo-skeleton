<?php
defined('TYPO3_MODE') or die('Access denied.');

call_user_func(function ($extension) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'Redkiwi.' . $extension,
        'Ajax',
        'AjaxCalls'
    );
}, $_EXTKEY);
