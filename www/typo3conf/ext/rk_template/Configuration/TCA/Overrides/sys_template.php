<?php
defined('TYPO3_MODE') or die('Access denied.');

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(function ($extension, $table) {

    ExtensionManagementUtility::addStaticFile(
        $extension,
        'Configuration/TypoScript',
        'Redkiwi Template'
    );

}, 'rk_template', 'sys_template');