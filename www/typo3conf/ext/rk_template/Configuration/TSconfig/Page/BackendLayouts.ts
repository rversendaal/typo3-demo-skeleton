mod.web_layout.BackendLayouts {

    default {
        title = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.default
        config {
            backend_layout {
                colCount = 3
                rowCount = 2
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.default.column1
                                colPos = 0
                            }
                            2 {
                                name = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.default.column1
                                colPos = 1
                                colspan = 2
                            }
                        }
                    }
                    2 {
                        columns {
                            1 {
                                name = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.default.column1
                                colPos  = 2
                            }
                            2 {
                                name = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.default.column1
                                colPos = 3
                            }
                            3 {
                                name = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.default.column1
                                colPos = 4
                            }
                        }
                    }
                }
            }
        }
    }
    single {
        title = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.single
        config {
            backend_layout {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.single.main
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }
    }
    layout_elements {
        title = LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.layout_elements
        config {
            backend_layout {
                colCount = 3
                rowCount = 3
                rows {
                    1 {
                        columns {
                            1 {
                                name =  LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.layout_elements.logo
                                colPos = 101
                            }
                            2 {
                                name =  LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.layout_elements.navigation
                                colPos = 102
                                colspan = 2
                            }
                        }
                    }
                    2 {
                        columns {
                            1 {
                                name =  LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.layout_elements.content_extra
                                colPos = 103
                                colspan = 3
                            }
                        }
                    }
                    3 {
                        columns {
                            1 {
                                name =  LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.layout_elements.footer_left
                                colPos = 104
                            }
                            2 {
                                name =  LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.layout_elements.footer_mid
                                colPos = 105
                            }
                            3 {
                                name =  LLL:EXT:rk_template/Resources/Private/Language/Page/BackendLayout.xlf:layout.layout_elements.footer_right
                                colPos = 106
                            }
                        }
                    }
                }
            }
        }
    }

}
