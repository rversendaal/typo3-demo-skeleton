tx_gridelements {
    setup {
        # Two Columns
        2 {
            title = Two Columns
            description = Display your content within two flexible columns

            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Left
                                colPos = 101
                            }

                            2 {
                                name = Right
                                colPos = 102
                            }
                        }
                    }
                }
            }
        }

        # Three Columns
        3 {
            title = Three Columns
            description = Display your content within three flexible columns

            config {
                colCount = 3
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Left
                                colPos = 101
                            }

                            2 {
                                name = Center
                                colPos = 102
                            }

                            3 {
                                name = Right
                                colPos = 103
                            }
                        }
                    }
                }
            }
        }

    }
}
