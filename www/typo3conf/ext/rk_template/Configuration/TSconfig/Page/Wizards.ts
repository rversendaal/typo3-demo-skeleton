temp.wizards < mod.wizards.newContentElement.wizardItems

mod.wizards.newContentElement.wizardItems {
    # Reset stored items for ordering
    common >
    special >
    forms >
    plugins >

    # Add custom elements for Redkiwi first
    redkiwi {
        header = LLL:EXT:rk_template/Resources/Private/Language/Model/ContentElements.xlf:wizard.title
        elements {
        }

        show = *
    }

    # Now restore wizard items
    common < temp.wizards.common
    special < temp.wizards.special
    forms < temp.wizards.forms
    plugins < temp.wizards.plugins
}
