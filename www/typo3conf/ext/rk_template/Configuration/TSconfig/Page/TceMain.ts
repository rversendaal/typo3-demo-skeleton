TCEMAIN {
    permissions {
        # Add "Default Role" as group id
        groupid = 1

        # Add default all permissions to new pages, might want to add delete
        user = show, editcontent, edit, new
        group = show, editcontent, edit, new
    }
}
