config {
    # HTML rendering & processing
    doctype = html5
    disablePrefixComment = 1
    headerComment (
========================================================
Site creation and TYPO3 integration by Redkiwi Rotterdam
				www.redkiwi.nl
========================================================
    )
    removeDefaultJS = external
    spamProtectEmailAddresses = 0
    xhtml_cleaning = all
    xmlprologue = none

    # Nice generated URLs
    tx_realurl_enable = {$rk_template.realurl.enable}
    # Link & URL handling
    absRefPrefix = /
    prefixLocalAnchors = all
    fileTarget =
    simulateStaticDocuments = 0
    typolinkCheckRootline = 1
    typolinkEnableLinksAcrossDomains = 1
    uniqueLinkVars = 1

    # Page title
    noPageTitle = 1
    pageTitleFirst = 1
    pageTitleSeparator = |
    pageTitleSeparator.noTrimWrap = | | |

    # Language & locale handling
    sys_language_overlay = hideNonTranslated
    sys_language_mode = content_fallback
    sys_language_uid = {$rk_template.languages.nl.uid}
    language = {$rk_template.languages.nl.abbr}
    locale_all = {$rk_template.languages.nl.locale}
    htmlTag_langKey = {$rk_template.languages.nl.code}

    # Disable CSS compression
    compressCss = 0
    compressJs = 0
    minifyCss = 0
    minifyJs = 0
    concatenateCss = 0
    concatenateJs = 0

    # Allow indexing
    index_enable = 1

    # Character sets
    renderCharset = utf-8
    metaCharset = utf-8
}

[applicationContext = Development*]
    config {
        no_cache = 1
    }
[global]
