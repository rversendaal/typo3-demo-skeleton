popup = PAGE
popup {
    typeNum = 4513

    config {
        disableAllHeaderCode = 1
        xhtml_cleaning = 0
        admPanel = 0
        additionalHeaders = Content-Type: application/json
        no_cache = 1
        debug = 0
    }

    10 = USER
    10 {
        userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
        extensionName = RkTemplate
        pluginName = Ajax
        vendorName = Redkiwi
        controller = AjaxController
        action = popup
    }
}