page = PAGE
page {

    headerData {
        100 = TEXT
        100 {
            value (
                <link rel="apple-touch-icon" sizes="57x57" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-57x57.png">
                <link rel="apple-touch-icon" sizes="60x60" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-60x60.png">
                <link rel="apple-touch-icon" sizes="72x72" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-72x72.png">
                <link rel="apple-touch-icon" sizes="76x76" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-76x76.png">
                <link rel="apple-touch-icon" sizes="114x114" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-114x114.png">
                <link rel="apple-touch-icon" sizes="120x120" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-120x120.png">
                <link rel="apple-touch-icon" sizes="144x144" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-144x144.png">
                <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/favicon-144x144.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-152x152.png">
                <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/favicon-152x152.png" />
                <link rel="apple-touch-icon" sizes="180x180" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/apple-icon-180x180.png">
                <link rel="icon" type="image/png" sizes="192x192"  href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/android-icon-192x192.png">
                <link rel="icon" type="image/png" sizes="32x32" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="96x96" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/favicon-96x96.png">
                <link rel="icon" type="image/png" sizes="16x16" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/favicon-16x16.png">
                <link rel="manifest" href="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/manifest.json">
                <meta name="msapplication-TileColor" content="#ffffff">
                <meta name="msapplication-TileImage" content="/typo3conf/ext/rk_template/Resources/Public/Images/favicon/ms-icon-144x144.png">
                <meta name="theme-color" content="#ffffff">
            )
        }
    }

    meta {
        viewport = width=device-width, initial-scale=1
        description.field = description
        keywords.field = keywords
    }

    # CSS includes
    includeCSS {
        default = EXT:rk_template/Resources/Public/Styles/style-min.css
        bootstrap = https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css
    }

    includeJSFooterlibs {
        custom_main = EXT:rk_template/Resources/Public/js/main.min.js
    }

    bodyTagCObject = COA
    bodyTagCObject {
        wrap = <body |>
        10 = TEXT
        10 {
            noTrimWrap = | id="page-|"|
            field = uid
            data.intval = 1
        }

        20 = COA
        20 {
            stdWrap.noTrimWrap = | class="|"|

            10 = TEXT
            10 {
                override = home
                override.if.value.field = uid
                override.if.equals = {$rk_template.pages.root}
                value = default
            }

            20 = TEXT
            20 {
                noTrimWrap = | ||
                value = user-logged-in
                required = 1
                if.isTrue.data = TSFE:fe_user|user|username
            }
        }
    }

    5 = COA

    10 = FLUIDTEMPLATE
    10 {
        extbase {
            pluginName = PageRenderer
            controllerExtensionName = RkTemplate
            controllerVendorName = Redkiwi
            controllerName = Page
            controllerActionName = render
        }

        layoutRootPaths {
            10 = EXT:rk_template/Resources/Private/Layouts
        }

        partialRootPaths {
            10 = EXT:rk_template/Resources/Private/Partials
        }

        templateRootPaths {
            10 = EXT:rk_template/Resources/Private/Templates/Page
        }

        templateName.cObject = CASE
        templateName.cObject {
            key.data = levelfield:-1, backend_layout_next_level, slide
            key.override.field = backend_layout

            pagets__default = TEXT
            pagets__default.value = Default

            pagets__single = TEXT
            pagets__single.value = Default

            default = TEXT
            default.value = Default
        }

        settings < rk_template
    }

    15 = COA

    footerData {

    }
}
