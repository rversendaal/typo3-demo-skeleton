module.tx_yoastseo {
    settings {
        allowedDoktypes {
        }
    }
}


plugin.tx_yoastseo.view.layoutRootPaths.1 = EXT:rk_template/Resources/Private/Extensions/YoastSeo/Layouts/
plugin.tx_yoastseo.view.partialRootPaths.1 = EXT:rk_template/Resources/Private/Extensions/YoastSeo/Partials/
plugin.tx_yoastseo.view.templateRootPaths.1 = EXT:rk_template/Resources/Private/Extensions/YoastSeo/Templates/