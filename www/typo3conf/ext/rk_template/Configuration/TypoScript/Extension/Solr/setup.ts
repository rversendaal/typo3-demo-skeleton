plugin.tx_solr {
    ########################################################################################################################
    # General settings
    ########################################################################################################################

    general.baseWrap.wrap = |

    cssFiles {
        results =
        ui =
    }

    javascriptFiles {
        library =
        ui =
        ui.autocomplete =
        suggest =
    }

    # Templates
    templateFiles {
        pagebrowser = EXT:rk_template/Resources/Private/Extensions/Solr/Templates/Pagebrowser.html
        search = EXT:rk_template/Resources/Private/Extensions/Solr/Templates/Searchbox.html
        results = EXT:rk_template/Resources/Private/Extensions/Solr/Templates/Results.html
    }

    search.results.resultsPerPage = 9

    ########################################################################################################################
    # Custom language labels
    ########################################################################################################################

    # Example, check /solr/Resources/Private/Language/PluginResults.xml for label names,
    # or simply add your own and use it in html through ###LLL:custom###

    _LOCAL_LANG.nl {
        no_results_nothing_found = We hebben geen resultaat kunnen vinden.
        results_searched_for = voor &quot;@searchWord&quot;
        search_placeholder = Zoekterm
        text_first = eerste
        text_prev = vorige
        text_next = volgende
        text_last = laatste
    }

    ########################################################################################################################
    # Suggest
    ########################################################################################################################
    suggest = 0

    ########################################################################################################################
    # Search settings
    ########################################################################################################################
    search {
        query {
            allowEmptyQuery = 1
            initializeWithEmptyQuery = 1
            showResultsOfInitialEmptyQuery = 1
            showResultsOfInitialQuery = 1

            queryFields = title^20.0, content^20.0, keywords^2.0, tagsInline^1.0, titleExact^1.0
            minimumWordLength = 3
        }

        results {
            fieldRenderingInstructions {
                content = TEXT
                content {
                    field = content
                    cropHTML = 800|...|1
                }
            }

            resultsHighlighting >
            resultsPerPage = 6

            pagebrowser {
                enabled = 1

                pagesBefore = 2
                pagesAfter = 2

                enableMorePages = 1
                enableLessPages = 1
            }
        }

        # Search facets
        faceting = 1
        faceting.facets {
        }
    }

    ########################################################################################################################
    # Additional page fields
    ########################################################################################################################
    index {
        enablePageIndexing = 1

        fieldProcessingInstructions {
        }

        queue {
            pages {
                fields {
                }
            }
        }
    }
}