plugin.tx_solr.solr {
    host = localhost
    port = 8983
    path = /solr/redkiwi-nl_NL/
}

[applicationContext = Development*]
    plugin.tx_solr {
        solr {
            host = localhost
            port = 8983
            path = /solr/vagrant-nl_NL/
        }
    }
[global]
