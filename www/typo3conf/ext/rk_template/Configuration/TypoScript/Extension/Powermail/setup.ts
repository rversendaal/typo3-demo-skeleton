plugin.tx_powermail {
    settings.setup {

        receiver.overwrite.senderEmail = TEXT
        receiver.overwrite.senderEmail.value = {$rk_template.email.noreply}

        spamshield.indicator.honeypod = {$plugin.tx_powermail.settings.setup.spamshield.indicator.honeypod}

        styles.framework {
            formClasses = default-form
            fieldWrappingClasses = powermail_field field-wrapper
        }

    }
}
