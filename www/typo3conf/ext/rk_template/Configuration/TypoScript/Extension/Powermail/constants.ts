plugin.tx_powermail {
    view {
        templateRootPath = EXT:rk_template/Resources/Private/Extensions/Powermail/Templates
        partialRootPath = EXT:rk_template/Resources/Private/Extensions/Powermail/Partials
        layoutRootPath = EXT:rk_template/Resources/Private/Extensions/Powermail/Layouts
    }

    settings {
        javascript.addAdditionalJavaScript = 0

        setup {
            spamshield.indicator.honeypod = 1
        }
    }

}
