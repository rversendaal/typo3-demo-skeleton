plugin.tx_rktemplate {
    view {
        templateRootPaths {
            10 = EXT:rk_template/Resources/Private/Templates
        }

        partialRootPaths {
            20 = EXT:rk_template/Resources/Private/Extensions/Solr/Fluid/Partials
            50 = EXT:rk_template/Resources/Private/Partials
        }

        layoutRootPaths {
            10 = EXT:rk_template/Resources/Private/Layouts
        }
    }

    persistence {
        storagePid = {$folders.root}
        recursive = 2
    }

    settings {
        # Don't touch this (this is just to let the extension know, that there is TypoScript included)
        staticTemplate = 1

        pages < rk_template.pages
        folders < rk_template.folders

        visual {
            default = 1
        }
    }
}
