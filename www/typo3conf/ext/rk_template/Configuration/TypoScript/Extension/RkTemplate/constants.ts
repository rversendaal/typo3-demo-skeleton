plugin.tx_rktemplate {
    view {
        # cat=plugin.tx_rktemplate/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:rk_template/Resources/Private/Templates
        # cat=plugin.tx_rktemplate/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:rk_template/Resources/Private/Layouts
        # cat=plugin.tx_rktemplate/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:rk_template/Resources/Private/Partials
    }
}
