temp.gridelements.fluid = FLUIDTEMPLATE
temp.gridelements.fluid {
    extbase {
        pluginName = Gridelements
        controllerExtensionName = RkTemplate
        controllerVendorName = Redkiwi
        controllerName = ContentElement
        controllerActionName = render
    }

    layoutRootPaths {
        10 = EXT:rk_template/Resources/Private/Extensions/GridElementLayout/Layouts
    }

    partialRootPaths {
        10 = EXT:rk_template/Resources/Private/Extensions/GridElementLayout/Partials
    }

    templateRootPaths {
        10 = EXT:rk_template/Resources/Private/Extensions/GridElementLayout/Templates
    }
}

tt_content.gridelements_pi1.20.10.setup {
    2 < lib.gridelements.defaultGridSetup
    2 {
        cObject < temp.gridelements.fluid
        cObject.templateName = TwoColumn
    }
}


tt_content.gridelements_pi1.20.10.setup {
    3 < lib.gridelements.defaultGridSetup
    3 {
        cObject < temp.gridelements.fluid
        cObject.templateName = ThreeColumn
    }
}
