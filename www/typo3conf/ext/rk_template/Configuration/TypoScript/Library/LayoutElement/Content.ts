lib.layoutElement.content {
    column < styles.content.get
    column {
        stdWrap {
            required = 1
            outerWrap.field = wrap
        }

        select {
            where >
            where {
                wrap = colPos = |
                field = column
                intval = 1
            }

            max.override.field = limit
        }
    }

    slide < .column
    slide {
        slide >
        slide = -1
        select.andWhere.dataWrap >
    }

    template < styles.content.get
    template {
        stdWrap {
            required = 1
            outerWrap.field = wrap
        }

        select {
            where >
            where {
                wrap = colPos = |
                field = column
                intval = 1
            }
            pidInList = {$rk_template.pages.layoutElements}
            max.override.field = limit
        }
    }
}