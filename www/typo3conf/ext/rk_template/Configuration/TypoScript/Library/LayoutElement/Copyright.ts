lib.layoutElement.copyright = COA
lib.layoutElement.copyright {
    10 = TEXT
    10.value = Copyright &copy;
    10.noTrimWrap = || |

    20 < .10
    20 {
        value = {$rk_template.website.created}
        wrap = |-
        if {
            value.data = date:U
            value.strftime = %Y
            isLessThan = {$rk_template.website.created}
        }
    }

    30 < .10
    30 {
        value >
        data = date:U
        strftime = %Y
    }

    40 < .10
    40.value = {$rk_template.website.name}
}
