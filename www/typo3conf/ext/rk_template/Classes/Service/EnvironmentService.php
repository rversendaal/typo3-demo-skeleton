<?php
namespace Redkiwi\RkTemplate\Service;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class EnvironmentService
 * @package Redkiwi\RkTemplate\Service
 */
class EnvironmentService implements \TYPO3\CMS\Core\SingletonInterface
{
    /** @var \TYPO3\CMS\Core\Core\ApplicationContext */
    private $applicationContext;

    /** @var */
    private $rootContext;

    /** @var */
    private $subContext;

    /**
     * EnvironmentService constructor.
     * @param GeneralUtility $generalUtility
     */
    public function __construct(GeneralUtility $generalUtility)
    {
        if (is_null($this->applicationContext)) {
            $this->applicationContext = $generalUtility::getApplicationContext();
        }
        $this->setContexts();

        return $this->applicationContext;
    }

    /**
     * @param string $rootContext
     */
    public function setRootContext(string $rootContext)
    {
        $this->rootContext = $rootContext;
    }

    /**
     * @param string $subContext
     */
    public function setSubContext(string $subContext)
    {
        $this->subContext = $subContext;
    }

    /**
     * Sets the rootContext and subContext according to the current applicationContext
     */
    private function setContexts()
    {
        $applicationContextString = (string) $this->applicationContext;
        $this->setRootContext($applicationContextString);

        if (strpos($applicationContextString, '/')) {
            $contextParts = explode('/', $applicationContextString);

            $this->setRootContext($contextParts[0]);
            $this->setSubContext($contextParts[1]);
        }
    }
    /**
     * @param string $subContext
     * @return bool
     */
    public function isDevelopment(string $subContext = ''): bool
    {
        return $this->isContext('Development', $subContext);
    }

    /**
     * @param string $subContext
     * @return bool
     */
    public function isTesting(string $subContext = ''): bool
    {
        return $this->isContext('Testing', $subContext);
    }

    /**
     * @param string $subContext
     * @return bool
     */
    public function isProduction(string $subContext = ''): bool
    {
        return $this->isContext('Production', $subContext);
    }

    /**
     * @param string $rootContext
     * @param string $subContext
     * @return bool
     */
    private function isContext(string $rootContext, string $subContext): bool
    {
        if (!empty($subContext)) {
            return ($this->rootContext === $rootContext && $this->subContext === $subContext);
        }

        return $this->rootContext === $rootContext;
    }

    /**
     * @param string $applicationContextString
     * @return bool
     */
    public function isEnvironment(string $applicationContextString): bool
    {
        return $this->applicationContext === $applicationContextString;
    }
}
