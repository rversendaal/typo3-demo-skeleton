<?php
namespace Redkiwi\RkTemplate\Controller;

use Redkiwi\RkTemplate\Service\EnvironmentService;
use Redkiwi\RkTemplate\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class AjaxController
 * @package Redkiwi\RkTemplate\Controller
 */
class AjaxController extends ActionController
{
    /** @var EnvironmentService */
    protected $environmentService;

    /**
     * AjaxController constructor.
     * @param EnvironmentService $environmentService
     */
    public function __construct(EnvironmentService $environmentService)
    {
        parent::__construct();

        $this->environmentService = $environmentService;
    }

    /**
     *
     */
    public function postAction()
    {

        $values = [
            $this->environmentService->isDevelopment(),       // true
            $this->environmentService->isDevelopment('Local'),  // true
            $this->environmentService->isTesting(''),// false
            $this->environmentService->isEnvironment('Testing/Acceptance')
        ];

        // A print_r wrapped in <pre> tags
        DebuggerUtility::pprint_r($values);

        // Dumps variables and dies
        DebuggerUtility::dd($values);

        exit;
    }
}
