<?php
namespace Redkiwi\RkTemplate\Utility;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility as BaseDebuggerUtility;

/**
 * Class DebuggerUtility
 * @package Redkiwi\RkTemplate\Utility
 */
class DebuggerUtility extends BaseDebuggerUtility
{
    /**
     * Extends the default var_dump by dying at the end.
     * Handy for certain blocking page views appearing in front of the dump.
     *
     */
    public static function dd() {
        parent::var_dump(func_get_args()); die();
    }

    /**
     * Prints an array while wrapping in a pre tag.
     * @param $variable
     */
    public static function pprint_r($variable)
    {
        echo '<pre>';
        print_r($variable);
        echo '</pre>';
    }


}
