<?php
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = [
    '_DEFAULT' => [
        'init' => [
            'appendMissingSlash' => 'ifNotFile,redirect',
            'emptyUrlReturnValue' => '/',
        ],
        'pagePath' => [
            'spaceCharacter' => '-',
            'languageGetVar' => 'L',
            'rootpage_id' => 1,
        ],
        'preVars' => [
            [
                'GETvar' => 'L',
                'valueMap' => [
                    'nl' => '1',
                ],
                'noMatch' => 'bypass',
            ],
        ],
        'fixedPostVars' => [
            'search' => [
                [
                    'GETvar' => 'tx_solr[page]'
                ]
            ],
        ],
        'postVarSets' => [
            '_DEFAULT' => [
                'extbaseParameters' => [
                    [
                        'GETvar' => '__[controller]',
                        'noMatch' => 'bypass'
                    ],
                ],
            ],
        ],
        'fileName' => [
            'defaultToHTMLsuffixOnPrev' => false,
            'acceptHTMLsuffix' => true,
            'index' => [
                'print' => [
                    'keyValues' => [
                        'type' => 98,
                    ],
                ],
            ],
        ],
    ],
];
