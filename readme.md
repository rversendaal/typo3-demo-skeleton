install using the following command: 

```
composer create-project redkiwi/typo3-skeleton --repository-url=https://composer.toolscloud.nl/ -s dev {optional directory}
```

Local setup: Run this script from the `root` of the project:
```
vagrant up
```

Make sure you have the following vagrant plugins:
+ vagrant plugin install vagrant-host-shell
+ vagrant plugin install vagrant-hostsupdater