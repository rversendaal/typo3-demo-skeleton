module.exports = function (gulp, plugins, paths) {
    return function () {

        return gulp.src(paths.src.svg + '/**/*.svg', {cwd: ''})
            .pipe(plugins.svgSprite({mode: {symbol: true}}))
            .pipe(gulp.dest(paths.dist.svg));

    };
};
