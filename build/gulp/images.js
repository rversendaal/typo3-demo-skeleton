module.exports = function (gulp, plugins, paths) {
    return function () {
        var srcDir = paths.src.images;

        return gulp.src(srcDir + '/**/*')
            .pipe(plugins.newer(paths.dist.images))
            .pipe(
                plugins.imagemin(
                    {
                        progressive: true,
                        interlaced: true,
                        multipass: true
                    }
                )
            )
            .pipe(gulp.dest(paths.dist.images));
    };
};