module.exports = function (gulp, plugins, paths, config) {
    return function () {

        return gulp.src(paths.src.admin + '/**/*.scss')
            .pipe(plugins.sass({
                outputStyle: 'compressed',
                precision: 10
            }).on('error', plugins.sass.logError))
            .pipe(plugins.autoprefixer(config.browsers))
            .pipe(plugins.cssnano({discardComments: {removeAll: true}, zindex: false}))
            .pipe(plugins.rename({suffix: '.min'}))
            .pipe(gulp.dest(paths.dist.admin));

    };
};
