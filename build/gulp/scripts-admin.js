module.exports = function (gulp, plugins, paths) {

    return function () {

        return gulp.src(paths.src.admin + '/**/*.js')
            .pipe(plugins.rename({suffix: '.min'}))
            .pipe(plugins.streamify(plugins.uglify({mangle: false})))
            .pipe(gulp.dest(paths.dist.admin));

    }

};
