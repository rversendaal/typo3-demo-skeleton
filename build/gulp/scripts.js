module.exports = function (gulp, plugins, paths, config) {

    return function () {

        var source = require('vinyl-source-stream');
        var browserify = require('browserify');
        var globalShim = require('browserify-global-shim').configure({
            'jquery': 'jQuery',
            'google': 'google'
        });

        if (config.production) {

            return browserify(paths.src.js + '/main.js')
                .transform(globalShim)
                .bundle()
                .pipe(source('main.js'))
                .pipe(plugins.rename({suffix: '.min'}))
                .pipe(plugins.streamify(plugins.uglify({mangle: false})))
                .pipe(gulp.dest(paths.dist.js));

        } else {

            return browserify(paths.src.js + '/main.js', { '_debug': true } )
                .transform(globalShim)
                .bundle()
                .pipe(source('main.js'))
                .pipe(plugins.rename({suffix: '.min'}))
                .pipe(gulp.dest(paths.dist.js))
                .pipe(plugins.browserSync.stream());

        }

    }

};
