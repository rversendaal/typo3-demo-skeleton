module.exports = function (gulp, plugins, paths) {

    return function () {

        return plugins.streamqueue({objectMode: true},
            gulp.src(paths.src.js + '/lib.js'))
            .pipe(plugins.include())
            .pipe(plugins.concat('libs.min.js'))
            .pipe(plugins.uglify({mangle: false}))
            .pipe(gulp.dest(paths.dist.js));

    }

};
