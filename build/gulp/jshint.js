module.exports = function (gulp, plugins, paths) {
    return function () {
        return gulp.src(paths.src.js + '/**/*.js')
            .pipe(plugins.jshint())
            .pipe(plugins.jshint.reporter('jshint-stylish'))
            .pipe(plugins.jshint.reporter('fail'))
    };
};
