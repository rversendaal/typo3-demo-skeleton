module.exports = function (gulp, plugins, paths) {
    return function () {

        return gulp.src(paths.src.fonts + '/**/*.{eot,svg,ttf,woff,woff2}')
            .pipe(gulp.dest(paths.dist.fonts));

    };
};
