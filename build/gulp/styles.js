module.exports = function (gulp, plugins, paths, config) {
    return function () {

        if (config.production) {

            return gulp.src(paths.src.css + '/style.scss')
                .pipe(plugins.sassBulkImport())
                .pipe(plugins.sass({
                    outputStyle: 'compressed',
                    precision: 10
                }).on('error', plugins.sass.logError))
                .pipe(plugins.autoprefixer(config.browsers))
                .pipe(plugins.cssnano({discardComments: {removeAll: true}, zindex: false}))
                .pipe(plugins.rename({suffix: '.min'}))
                .pipe(gulp.dest(paths.dist.css));

        } else {

            return gulp.src(paths.src.css + '/style.scss')
                .pipe(plugins.sassBulkImport())
                .pipe(plugins.sourcemaps.init())
                .pipe(
                    plugins.sass({
                    outputStyle: 'compact',
                    precision: 10
                }).on('error', plugins.sass.logError))
                .pipe(plugins.autoprefixer(config.browsers))
                .pipe(plugins.rename({suffix: '.min'}))
                .pipe(plugins.sourcemaps.write())
                .pipe(gulp.dest(paths.dist.css))
                .pipe(plugins.browserSync.stream());

        }

    };
};
