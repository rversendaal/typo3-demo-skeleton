// Import dependencies
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({camelize: true});
var config = require('./config.json');
var del = require('del');
var fileExists = require('file-exists');
var merge = require('merge-stream');
var runSequence = require('run-sequence');
var streamqueue = require('streamqueue');


// Setup plugins
plugins.del = del;
plugins.fileExists = fileExists;
plugins.merge = merge;
plugins.streamqueue = streamqueue;

// Development setup
if (!config.production) {
    var browserSync = require('browser-sync');

    plugins.browserSync = browserSync.create();
}

var srcPath = '../src/' + config.theme_dirname;
var distPath = '../www/wp-content/themes/' + config.theme_dirname + '/dist';

if (config.platform === 'typo3') {

    distPath = '../www/typo3conf/ext/' + config.theme_dirname + '/Resources/Public';

} else if (config.platform === 'magento') {

    //  "In a far away galaxy, in a perfect world" - Unknown developer

}

// Setup theme paths
var paths = {
    src: {
        admin: srcPath + '/admin',
        css: srcPath + '/scss',
        fonts: srcPath + '/fonts',
        images: srcPath + '/img',
        svg: srcPath + '/svg',
        js: srcPath + '/js',
        root: srcPath
    },
    dist: {
        admin: distPath + '/admin',
        css: distPath + '/css',
        fonts: distPath + '/fonts',
        images: distPath + '/img',
        svg: distPath + '/svg',
        js: distPath + '/js',
        root: distPath
    }
};

function getTask(task) {
    return require('./gulp/' + task)(gulp, plugins, paths, config);
}

// Setup default tasks
gulp.task('clean', getTask('clean'));
gulp.task('fonts', getTask('fonts'));
gulp.task('images', getTask('images'));
gulp.task('svg-sprite', getTask('svg-sprite'));
gulp.task('scripts', getTask('scripts'));
gulp.task('lib-scripts', getTask('lib-scripts'));
gulp.task('styles', getTask('styles'));

if (config.platform === 'wordpress') {

    gulp.task('scripts-admin', getTask('scripts-admin'));
    gulp.task('styles-admin', getTask('styles-admin'));

}

// Setup development tasks
if (!config.production) {
    gulp.task('jshint', getTask('jshint'));
    gulp.task('reload', getTask('reload'));

    // Compile and serve files
    gulp.task(
        'watch', function () {

            plugins.browserSync.init(
                {
                    notify: {
                        styles: {
                            top: 'auto',
                            bottom: '0'
                        }
                    },
                    port: 3000,
                    proxy: config.url
                }
            );

            gulp.watch('**/*.{scss,css}', {cwd: paths.src.css}, ['styles']);
            gulp.watch('**/*.js', {cwd: paths.src.js}, ['jshint', 'scripts', 'lib-scripts'], 'reload');
            gulp.watch('**/*', {cwd: paths.src.fonts}, ['fonts'], 'reload');
            gulp.watch('**/*', {cwd: paths.src.images}, ['images'], 'reload');
            gulp.watch('**/*', {cwd: paths.src.svg}, ['svg-sprite'], 'reload');

            if (config.platform === 'wordpress') {

                gulp.watch('**/*', {cwd: paths.src.admin}, ['scripts-admin', 'styles-admin'], 'reload');

            }

        }
    );
}

//  Compile files and launch development area
gulp.task(
    'default', function (callback) {
        if (!config.production) {
            return runSequence(
                'deploy',
                'watch',
                callback
            );
        } else {
            return runSequence(
                'deploy',
                callback
            );
        }
    }
);

//  Deploy production files
gulp.task(
    'deploy', function (callback) {

        plugins.msg.Info('*', 'FrontEnd build in progress');
        plugins.msg.Info('Platform: ' + config.platform);
        plugins.msg.Info('Theme: ' + config.theme_dirname);
        plugins.msg.Info('Mode: ' + (config.production ? 'Production' : 'Development'));

        var tasks = ['styles', 'scripts', 'lib-scripts', 'fonts', 'images', 'svg-sprite'];

        if (config.platform === 'wordpress') {

            Array.prototype.push.apply(tasks, ['styles-admin', 'scripts-admin']);

        }

        return runSequence(
            'clean',
            tasks,
            callback
        );
    }
);
